package comiam.nsu.catdoggi.responces;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MessageResponse
{
    String message;
}
