package comiam.nsu.catdoggi.responces;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtResponse
{
    private String token;
}