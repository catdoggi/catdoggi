package comiam.nsu.catdoggi.responces;

import comiam.nsu.catdoggi.models.AnimalType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class AnimalPreviewResponse implements Serializable
{
    private int animalID;
    private String name;
    private String imgUrl;
    private boolean isLikedByCurrentUser;
    private AnimalType type;
}
