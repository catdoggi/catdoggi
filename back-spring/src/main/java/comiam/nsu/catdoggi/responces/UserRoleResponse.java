package comiam.nsu.catdoggi.responces;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserRoleResponse
{
    private boolean admin;
}
