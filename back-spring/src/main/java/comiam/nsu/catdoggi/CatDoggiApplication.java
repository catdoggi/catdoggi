package comiam.nsu.catdoggi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class CatDoggiApplication
{
	public static void main(String[] args) {
		SpringApplication.run(CatDoggiApplication.class, args);
	}
}
