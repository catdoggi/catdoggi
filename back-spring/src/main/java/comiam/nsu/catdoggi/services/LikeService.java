package comiam.nsu.catdoggi.services;

import comiam.nsu.catdoggi.models.Animal;
import comiam.nsu.catdoggi.models.Like;
import comiam.nsu.catdoggi.models.User;
import comiam.nsu.catdoggi.models.repos.LikeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LikeService
{
    @Autowired
    private LikeRepo likeRepo;
    @Autowired
    private UserService userService;

    public List<Like> getLikesOfCurrentUser(User user)
    {
        return  likeRepo.findLikeByUser(user);
    }

    public boolean userLikedThatAnimal(User user, Animal animal)
    {
        return likeRepo.findLikeByUserAndAnimal(user, animal) != null;
    }

    //если уже есть лайк, убираем его
    public boolean setLikeToAnimal(Animal animal)
    {
        if(animal == null || animal.getId() <= 0)
            return false;

        var user = userService.getLoggedInUser();

        if(user == null)
            return false; //мне кажется тут false логичнее

        var like = likeRepo.findLikeByUserAndAnimal(user, animal);
        if(like != null) {
            likeRepo.delete(like);
            return true;
        } else
        {
            try
            {
                var userReattached = userService.loadUserById(user.getId());
                likeRepo.save(new Like(userReattached, animal));
                return true;
            }catch (Throwable e)
            {
                e.printStackTrace();
                return false;
            }
        }
    }
}
