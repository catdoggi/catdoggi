package comiam.nsu.catdoggi.services;

import comiam.nsu.catdoggi.models.Role;
import comiam.nsu.catdoggi.models.User;
import comiam.nsu.catdoggi.models.repos.RoleRepo;
import comiam.nsu.catdoggi.models.repos.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserService implements UserDetailsService
{
    @Autowired
    UsersRepo usersRepo;
    @Autowired
    RoleRepo roleRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    private User testUser = null;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        var user = usersRepo.findByUsername(username);
        if (user == null)
            throw new UsernameNotFoundException("User not found");

        return user;
    }

    public boolean checkUserIsAdmin()
    {
        var user = getLoggedInUser();

        if (user == null)
            return false;

        return user.getRoles().stream().anyMatch(r -> r.getName().equals("ROLE_ADMIN"));
    }

    public User loadUserById(int id)
    {
        var user = usersRepo.findById(id);
        if (user == null)
            throw new UsernameNotFoundException("User not found");

        return user;
    }

    public boolean saveUser(User user)
    {
        var userFromDB = usersRepo.findByUsername(user.getUsername());
        if (userFromDB != null)
            return false;
        Role role = roleRepository.findByName("ROLE_USER");
        if (role == null) {
            role = new Role(roleRepository.count() + 1, "ROLE_USER");
            roleRepository.save(role);
        }
        user.setRoles(Collections.singleton(role));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setId(null);
        usersRepo.save(user);
        return true;
    }

    public User findByUsernameAndPassword(String username, String password)
    {
        var user = usersRepo.findByUsername(username);
        if (user != null && bCryptPasswordEncoder.matches(password, user.getPassword()))
            return user;

        return null;
    }

    public User getLoggedInUser()
    {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) return testUser;
        if (authentication.getPrincipal().equals("anonymousUser"))
        {
            return testUser;
        }
        return (User) authentication.getPrincipal();
    }

    //только для тестов
    public void setTestUser(User user)
    {
        if (user == null)
        {
            testUser = null;
            return;
        }
        saveUser(user);
        testUser = usersRepo.findByUsername(user.getUsername());
    }
}
