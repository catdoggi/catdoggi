package comiam.nsu.catdoggi.services;

import comiam.nsu.catdoggi.models.Feature;
import comiam.nsu.catdoggi.models.repos.FeatureRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeatureService
{
    @Autowired
    private FeatureRepo featureRepo;

    public void saveFeature(Feature feature)
    {
        featureRepo.save(feature);
    }

    public Iterable<Feature> getFeatures()
    {
        return featureRepo.findAll();
    }

    public Feature findFeatureByName(String name)
    {
        return featureRepo.findFeatureByFeature(name).orElse(null);
    }
}
