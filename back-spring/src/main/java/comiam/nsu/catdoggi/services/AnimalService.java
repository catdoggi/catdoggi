package comiam.nsu.catdoggi.services;

import comiam.nsu.catdoggi.models.*;
import comiam.nsu.catdoggi.models.repos.*;
import comiam.nsu.catdoggi.request.AnimalData;
import comiam.nsu.catdoggi.responces.AnimalPreviewResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class AnimalService
{
    @Autowired
    private AnimalRepo animalRepo;
    @Autowired
    private FeatureService featureService;
    @Autowired
    private BehaviourRepo behaviourRepo;
    @Autowired
    private HealthRepo healthRepo;
    @Autowired
    private CareRepo careRepo;
    @Autowired
    private LikeRepo likeRepo;
    @Autowired
    private LikeService likeService;
    @Autowired
    private UserService userService;

    @Transactional
    public boolean deleteAnimal(int id)
    {
        var animal = getAnimalByID(id, false);

        if (animal == null)
            return false;
        else
        {
            likeRepo.deleteLikesByAnimal(animal);
            animalRepo.delete(animal);
            return true;
        }
    }

    public boolean saveAnimal(AnimalData animalData)
    {
        try
        {
            var hd = new HealthDescription(0, animalData.getHealthDescription());
            var bd = new BehaviourDescription(0, animalData.getBehaviourDescription());
            var cd = new CareDescription(0, animalData.getCareDescription());
            Set<Feature> featureSet = new HashSet<>();

            for (var feature : animalData.getFeatures())
            {
                var featureFromDB = featureService.findFeatureByName(feature);

                if (featureFromDB == null)
                {
                    featureFromDB = new Feature(0, feature, null);
                    featureService.saveFeature(featureFromDB);
                }

                featureSet.add(featureFromDB);
            }

            behaviourRepo.save(bd);
            healthRepo.save(hd);
            careRepo.save(cd);

            var newAnimal = new Animal(animalData.getName(), animalData.getType(),
                    animalData.getUrl(), animalData.getAverageWeight(), animalData.getAverageAge(),
                    hd, cd, bd);
            newAnimal.setFeatures(featureSet);

            animalRepo.save(newAnimal);
            return true;
        } catch (Throwable e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public Iterable<AnimalPreviewResponse> getCats()
    {
        var animalList = animalRepo.getAnimalByType(AnimalType.CAT);
        return getAnimalPreviews(animalList);
    }

    public Iterable<AnimalPreviewResponse> getDogs()
    {
        var animalList = animalRepo.getAnimalByType(AnimalType.DOG);
        return getAnimalPreviews(animalList);
    }

    private Iterable<AnimalPreviewResponse> getAnimalPreviews(List<Animal> animalList)
    {
        var previews = new ArrayList<AnimalPreviewResponse>();
        var user = userService.getLoggedInUser();
        boolean checkLikes = user != null;

        List<Like> userLikes = null;

        if (checkLikes)
            userLikes = likeService.getLikesOfCurrentUser(user);

        for (var animal : animalList)
            previews.add(new AnimalPreviewResponse(animal.getId(), animal.getName(), animal.getUrl(),
                    checkLikes && animaLikedByUser(animal, userLikes), animal.getType()));

        return previews;
    }

    public Iterable<AnimalPreviewResponse> findAnimalsByFeatures(String s, String animalType) {
        var result = new LinkedList<Animal>();
        AnimalType type;
        try {
            type = AnimalType.valueOf(animalType);
        } catch (IllegalArgumentException e) {
            return getAnimalPreviews(result);
        }
        var allFeatureList = featureService.getFeatures();
        for (var f : allFeatureList) {
            if (f.getFeature().equalsIgnoreCase(s)) {
                var set = f.getAnimals();
                for (var animal : set) {
                    if (type == animal.getType()) {
                        result.add(animal);
                    }
                }
                break;
            }
        }
        return getAnimalPreviews(result);
    }


    private boolean animaLikedByUser(Animal animal, List<Like> likes)
    {
        return likes.stream().anyMatch(l -> l.getAnimal().equals(animal));
    }

    public Animal getAnimalByID(int id, boolean findLikes)
    {
        var animal = animalRepo.findById(id);

        User user;
        if (findLikes)
        {
            if (animal.isPresent() && (user = userService.getLoggedInUser()) != null)
            {
                animal.get().setLiked(likeService.userLikedThatAnimal(user, animal.get()));
                return animal.get();
            } else
                return animal.orElse(null);
        } else
            return animal.orElse(null);
    }

    public AnimalData simplifyAnimalEntity(Animal animal)
    {
        if (animal == null)
            return null;
        var featureList = new ArrayList<String>();

        if (animal.getFeatures() != null)
            for (var feature : animal.getFeatures())
                featureList.add(feature.getFeature());

        return new AnimalData(animal.getName(), animal.isLiked(), animal.getType(), animal.getUrl(), animal.getAverageWeight(),
                animal.getAverageAge(), animal.getHealthDescription().getName(), animal.getCareDescription().getName(),
                animal.getBehaviourDescription().getName(), featureList);
    }

    public Iterable<AnimalPreviewResponse> getFavorites()
    {
        var user = userService.getLoggedInUser();
        if (user == null) return new ArrayList<>();
        var likeList = likeService.getLikesOfCurrentUser(user);
        var animalList = new ArrayList<Animal>();
        for (var like : likeList)
        {
            animalList.add(like.getAnimal());
        }
        return getAnimalPreviews(animalList);
    }
}
