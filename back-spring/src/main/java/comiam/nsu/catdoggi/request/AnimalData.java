package comiam.nsu.catdoggi.request;

import comiam.nsu.catdoggi.models.AnimalType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AnimalData
{
    private String name;
    private boolean liked;
    private AnimalType type;
    private String url;
    private String averageWeight;
    private String averageAge;
    private String healthDescription;
    private String careDescription;
    private String behaviourDescription;
    private List<String> features;
}
