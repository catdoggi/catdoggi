package comiam.nsu.catdoggi.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegistrationRequest {
    String username;
    String password;
    String confirmPassword;
}
