package comiam.nsu.catdoggi.controllers;

import comiam.nsu.catdoggi.request.AnimalData;
import comiam.nsu.catdoggi.responces.MessageResponse;
import comiam.nsu.catdoggi.services.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController
{
    @Autowired
    private AnimalService animalService;

    @PostMapping("/animal/add")
    public @ResponseBody
    ResponseEntity<MessageResponse> addAnimal(@RequestBody AnimalData animalData)
    {
        return animalService.saveAnimal(animalData) ? ResponseEntity.ok().body(new MessageResponse("Success")) :
                ResponseEntity.badRequest().body(new MessageResponse("Error: can't save animal!"));
    }

    @PostMapping("/animal/remove/{id}")
    public @ResponseBody
    ResponseEntity<MessageResponse> deleteAnimal(@PathVariable int id)
    {
        return animalService.deleteAnimal(id) ? ResponseEntity.ok().body(new MessageResponse("Success")) :
                ResponseEntity.badRequest().body(new MessageResponse("Error: can't delete animal!"));
    }
}