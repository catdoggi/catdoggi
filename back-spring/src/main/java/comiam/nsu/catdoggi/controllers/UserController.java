package comiam.nsu.catdoggi.controllers;

import comiam.nsu.catdoggi.responces.UserRoleResponse;
import comiam.nsu.catdoggi.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController
{
    @Autowired
    private UserService userService;

    @GetMapping("/role")
    public @ResponseBody
    ResponseEntity<UserRoleResponse> isAdmin()
    {
        return ResponseEntity.ok().body(new UserRoleResponse(userService.checkUserIsAdmin()));
    }
}
