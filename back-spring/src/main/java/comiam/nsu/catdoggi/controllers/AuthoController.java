package comiam.nsu.catdoggi.controllers;

import comiam.nsu.catdoggi.jwt.JwtProvider;
import comiam.nsu.catdoggi.models.User;
import comiam.nsu.catdoggi.request.LoginRequest;
import comiam.nsu.catdoggi.request.RegistrationRequest;
import comiam.nsu.catdoggi.responces.JwtResponse;
import comiam.nsu.catdoggi.responces.MessageResponse;
import comiam.nsu.catdoggi.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
public class AuthoController
{
    @Autowired
    private UserService userService;
    @Autowired
    private JwtProvider jwtProvider;

    @GetMapping("/signup")
    public String registration()
    {
        return "signup";
    }

    @PostMapping(value = "/signup", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<MessageResponse> addUser(@Valid @RequestBody RegistrationRequest registrationRequest)
    {
        if (registrationRequest.getPassword() == null)
            return ResponseEntity.badRequest().body(new MessageResponse("Error: password is empty"));

        if (!registrationRequest.getPassword().equals(registrationRequest.getConfirmPassword())) //???
            return ResponseEntity.badRequest().body(new MessageResponse("Error: password mismatch"));

        var user = new User();
        user.setUsername(registrationRequest.getUsername());
        user.setPassword(registrationRequest.getPassword());
        user.setId(null);
        if (!userService.saveUser(user))
            return ResponseEntity.badRequest().body(new MessageResponse("Error: ser with the same name already exists"));

        return ResponseEntity.ok().body(new MessageResponse("success"));
    }

    @GetMapping("/signin")
    public String auth()
    {
        return "signin";
    }

    @PostMapping(value = "/signin", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<?> auth(@Valid @RequestBody LoginRequest request)
    {
        var user = userService.findByUsernameAndPassword(request.getUsername(), request.getPassword());
        if (user == null)
            return ResponseEntity.badRequest().body(new MessageResponse("Error"));
        String token = jwtProvider.generateToken(user.getId().toString());
        return ResponseEntity.ok().body(new JwtResponse(token));
    }
}
