package comiam.nsu.catdoggi.controllers;

import comiam.nsu.catdoggi.models.AnimalType;
import comiam.nsu.catdoggi.models.Feature;
import comiam.nsu.catdoggi.request.AnimalData;
import comiam.nsu.catdoggi.responces.AnimalPreviewResponse;
import comiam.nsu.catdoggi.responces.MessageResponse;
import comiam.nsu.catdoggi.services.AnimalService;
import comiam.nsu.catdoggi.services.FeatureService;
import comiam.nsu.catdoggi.services.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/animals")
@CrossOrigin
public class AnimalController
{
    @Autowired
    private LikeService likeService;
    @Autowired
    private AnimalService animalService;
    @Autowired
    private FeatureService featureService;

    @GetMapping("/cats")
    public @ResponseBody Iterable<AnimalPreviewResponse> getCats()
    {
        return animalService.getCats();
    }

    @GetMapping("/dogs")
    public @ResponseBody Iterable<AnimalPreviewResponse> getDogs()
    {
        return animalService.getDogs();
    }

    @PostMapping("/like/{id}")
    public @ResponseBody ResponseEntity<MessageResponse> like(@PathVariable int id)
    {
        return likeService.setLikeToAnimal(animalService.getAnimalByID(id, false)) ?
                ResponseEntity.ok().body(new MessageResponse("Success")) :
                ResponseEntity.badRequest().body(new MessageResponse("Error: can't like animal!"));
    }

    @GetMapping("/{id}")
    public @ResponseBody
    AnimalData getAnimal(@PathVariable int id)
    {
        return animalService.simplifyAnimalEntity(animalService.getAnimalByID(id, true));
    }

    @GetMapping("/search")
    public @ResponseBody Iterable<AnimalPreviewResponse> find(@Valid @RequestParam String features,
                                                              @Valid @RequestParam String type)
    {
        return animalService.findAnimalsByFeatures(URLDecoder.decode(features, StandardCharsets.UTF_8),
                URLDecoder.decode(type, StandardCharsets.UTF_8));
    }

    @GetMapping("/features")
    public @ResponseBody Iterable<Feature> getFeatures()
    {
        return featureService.getFeatures();
    }

    @GetMapping("/favorites")
    public @ResponseBody Iterable<AnimalPreviewResponse> getFavorites()
    {
        return animalService.getFavorites();
    }
}
