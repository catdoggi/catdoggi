package comiam.nsu.catdoggi.models;

public enum AnimalType
{
    CAT,
    DOG
}
