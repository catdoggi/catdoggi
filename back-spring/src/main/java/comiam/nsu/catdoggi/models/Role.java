package comiam.nsu.catdoggi.models;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

//Имя роли должно соответствовать шаблону: «ROLE_ИМЯ», например, ROLE_USER
@Entity
@Data
@Table(name = "ROLES")
@NoArgsConstructor
@AllArgsConstructor
public class Role implements GrantedAuthority, Serializable
{
    @Id
    private Long id;

    private String name;

    @Transient
    @ManyToMany(mappedBy = "ROLES")
    private Set<User> users;

    public Role(Long id)
    {
        this.id = id;
    }

    public Role(Long id, String name)
    {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getAuthority()
    {
        return getName();
    }
}