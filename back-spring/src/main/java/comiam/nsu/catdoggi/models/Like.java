package comiam.nsu.catdoggi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@Table(name = "LIKES")
@NoArgsConstructor
public class Like
{
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="like_id_seq")
    @SequenceGenerator(name="like_id_seq", sequenceName="like_id_seq", allocationSize=1)
    @Column(name = "LIKE_ID", nullable = false)
    @JsonIgnore
    private int id;

    @OneToOne
    @JoinColumn(name = "USER_ID")
    private User user;
    @OneToOne
    @JoinColumn(name = "ANIMAL_ID")
    private Animal animal;

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var likeID = (Like) o;
        return user.equals(likeID.user) && animal.equals(likeID.animal);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(user, animal);
    }

    public Like(User user, Animal animal)
    {
        this.user = user;
        this.animal = animal;
    }
}
