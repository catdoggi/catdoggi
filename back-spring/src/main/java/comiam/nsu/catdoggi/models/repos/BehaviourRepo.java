package comiam.nsu.catdoggi.models.repos;

import comiam.nsu.catdoggi.models.BehaviourDescription;
import org.springframework.data.repository.CrudRepository;

public interface BehaviourRepo extends CrudRepository<BehaviourDescription, Integer>
{
}
