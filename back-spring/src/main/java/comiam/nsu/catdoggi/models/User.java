package comiam.nsu.catdoggi.models;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@Table(name = "USERS")
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable, UserDetails
{
    @Id
    @Column(name = "USER_ID", nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_id_seq")
    @SequenceGenerator(name="user_id_seq", sequenceName="user_id_seq", allocationSize=1)
    private Integer id;

    @Column(name = "PASSWORD", nullable = false, length = 100) //такая длина, потому что сейчас мы храним в закодированном виде
    private String password;

    @Column(name = "NAME", nullable = false, length = 50)
    private String username;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles;

    public User(String password, String name)
    {
        this.password = password;
        this.username = name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}