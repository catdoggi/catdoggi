package comiam.nsu.catdoggi.models.repos;

import comiam.nsu.catdoggi.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UsersRepo extends CrudRepository<User, Integer>
{
    User findByUsername(String username);
    User findById(int id);
    Boolean existsByUsername(String username);
}
