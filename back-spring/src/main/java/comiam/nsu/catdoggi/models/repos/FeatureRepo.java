package comiam.nsu.catdoggi.models.repos;

import comiam.nsu.catdoggi.models.Feature;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface FeatureRepo extends CrudRepository<Feature, Integer>
{
    Optional<Feature> findFeatureByFeature(String feature);
}
