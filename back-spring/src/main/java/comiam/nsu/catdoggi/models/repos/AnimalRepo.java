package comiam.nsu.catdoggi.models.repos;

import comiam.nsu.catdoggi.models.Animal;
import comiam.nsu.catdoggi.models.AnimalType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalRepo extends CrudRepository<Animal, Integer>
{
    List<Animal> findAll();
    List<Animal> getAnimalByType(AnimalType type);
}
