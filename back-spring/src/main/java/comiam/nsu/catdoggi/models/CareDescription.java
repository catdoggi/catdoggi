package comiam.nsu.catdoggi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@Table(name = "CARE_DESCRIPTIONS")
@NoArgsConstructor
@AllArgsConstructor
public class CareDescription
{
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="care_id_seq")
    @SequenceGenerator(name="care_id_seq", sequenceName="care_id_seq", allocationSize=1)
    @Column(name = "CARE_DESCRIPTION_ID", nullable = false)
    @JsonIgnore
    private int id;

    @Column(name = "DESCRIPTION", nullable = false, length = 4000)
    private String name;

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CareDescription that = (CareDescription) o;
        return id == that.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }
}
