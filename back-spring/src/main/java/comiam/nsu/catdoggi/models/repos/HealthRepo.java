package comiam.nsu.catdoggi.models.repos;

import comiam.nsu.catdoggi.models.HealthDescription;
import org.springframework.data.repository.CrudRepository;

public interface HealthRepo extends CrudRepository<HealthDescription, Integer>
{
}
