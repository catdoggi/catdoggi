package comiam.nsu.catdoggi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.Calendar;
import java.util.Objects;

@Entity
@Data
@Table(name = "COMMENTS")
@NoArgsConstructor
@AllArgsConstructor
public class Comment
{
    @Id
    @Column(name = "COMMENT_ID", nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="comment_id_seq")
    @SequenceGenerator(name="comment_id_seq", sequenceName="comment_id_seq", allocationSize=1)
    @JsonIgnore
    private int id;

    @OneToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "COMMENT_TEXT", nullable = false, length = 1000)
    private String comment;

    @Column(name = "COMMENT_DATE", nullable = false)
    private Date date;

    public Comment(User user, String comment)
    {
        this.user = user;
        this.comment = comment;
        this.date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var comment = (Comment) o;
        return id == comment.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }
}
