package comiam.nsu.catdoggi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@Table(name = "FEATURES")
@NoArgsConstructor
@AllArgsConstructor
public class Feature
{
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="feature_id_seq")
    @SequenceGenerator(name="feature_id_seq", sequenceName="feature_id_seq", allocationSize=1)
    @Column(name = "FEATURE_ID", nullable = false)
    @JsonIgnore
    private int id;

    @Column(name = "FEATURE_NAME", length = 4000)
    private String feature;

    @ManyToMany(mappedBy = "features")
    @JsonIgnore
    private Set<Animal> animals;

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var feature = (Feature) o;
        return id == feature.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }
}
