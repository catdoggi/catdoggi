package comiam.nsu.catdoggi.models.repos;

import comiam.nsu.catdoggi.models.Animal;
import comiam.nsu.catdoggi.models.Like;
import comiam.nsu.catdoggi.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeRepo extends CrudRepository<Like, Integer>
{
    List<Like> findLikeByUser(User user);
    Like findLikeByUserAndAnimal(User user, Animal animal);
    void deleteLikesByAnimal(Animal animal);
}
