package comiam.nsu.catdoggi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@Table(name = "HEALTH_DESCRIPTIONS")
@NoArgsConstructor
@AllArgsConstructor
public class HealthDescription
{
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="health_id_seq")
    @SequenceGenerator(name="health_id_seq", sequenceName="health_id_seq", allocationSize=1)
    @Column(name = "HEALTH_DESCRIPTION_ID", nullable = false)
    @JsonIgnore
    private int id;

    @Column(name = "DESCRIPTION", nullable = false, length = 4000)
    private String name;

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (HealthDescription) o;
        return id == that.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }
}
