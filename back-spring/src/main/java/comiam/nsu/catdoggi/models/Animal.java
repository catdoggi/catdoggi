package comiam.nsu.catdoggi.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@Table(name = "ANIMALS")
@NoArgsConstructor
@AllArgsConstructor
public class Animal
{
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="animal_id_seq")
    @SequenceGenerator(name="animal_id_seq", sequenceName="animal_id_seq", allocationSize=1)
    @Column(name = "ANIMAL_ID", nullable = false)
    private int id;

    @Column(name = "NAME", nullable = false, unique = true, length = 80)
    private String name;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private AnimalType type;

    @Column(name = "IMAGE_URL", length = 4000)
    private String url;

    @Column(name = "AVG_WEIGHT", nullable = false, length = 200)
    private String averageWeight;

    @Column(name = "AVG_HEIGHT",nullable = false, length = 200)
    private String averageAge;

    @OneToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "HEALTH_DESC", nullable = false)
    private HealthDescription healthDescription;

    @OneToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "CARE_DESC", nullable = false)
    private CareDescription careDescription;

    @OneToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "BEHAVIOUR_DESC", nullable = false)
    private BehaviourDescription behaviourDescription;

    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "ANIMAL_COMMENTS",
            joinColumns = @JoinColumn(name = "ANIMAL_ID"),
            inverseJoinColumns = @JoinColumn(name = "COMMENT_ID"))
    private List<Comment> comments;

    @ManyToMany (fetch = FetchType.EAGER, cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE,
    })
    @JoinTable(name = "ANIMAL_FEATURES",
            joinColumns = @JoinColumn(name = "ANIMAL_ID"),
            inverseJoinColumns = @JoinColumn(name = "FEATURE_ID")
    )
    private Set<Feature> features;

    // used only for response
    @Transient
    private boolean liked;

    public Animal(String name, AnimalType type, String url, String averageWeight, String averageAge, HealthDescription healthDescription, CareDescription careDescription, BehaviourDescription behaviourDescription)
    {
        this.name = name;
        this.type = type;
        this.url = url;
        this.averageWeight = averageWeight;
        this.averageAge = averageAge;
        this.healthDescription = healthDescription;
        this.careDescription = careDescription;
        this.behaviourDescription = behaviourDescription;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var animal = (Animal) o;
        return id == animal.id && name.equals(animal.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name);
    }

    @Override
    public String toString()
    {
        return "Animal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", url='" + url + '\'' +
                ", averageWeight='" + averageWeight + '\'' +
                ", averageHeight='" + averageAge + '\'' +
                ", healthDescription=" + healthDescription +
                ", careDescription=" + careDescription +
                ", behaviourDescription=" + behaviourDescription +
                '}';
    }
}
