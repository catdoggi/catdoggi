package comiam.nsu.catdoggi.models.repos;

import comiam.nsu.catdoggi.models.CareDescription;
import org.springframework.data.repository.CrudRepository;

public interface CareRepo extends CrudRepository<CareDescription, Integer>
{
}
