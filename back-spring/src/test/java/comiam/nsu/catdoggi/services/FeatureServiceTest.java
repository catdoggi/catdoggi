package comiam.nsu.catdoggi.services;

import comiam.nsu.catdoggi.config.ServiceTestConfig;
import comiam.nsu.catdoggi.models.Feature;
import comiam.nsu.catdoggi.models.repos.FeatureRepo;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import(ServiceTestConfig.class)
class FeatureServiceTest
{
    @Autowired
    private FeatureService featureService;

    @Autowired
    private FeatureRepo featureRepo;

    @Test
    @Rollback(false)
    @Order(1)
    void testSaveFeature()
    {
        Feature feature0 = new Feature(0, "amogus", null);
        Feature feature1 = new Feature(0, "amebus", null);
        Feature feature2 = new Feature(0, "kenobus", null);
        featureService.saveFeature(feature0);
        featureService.saveFeature(feature1);
        featureService.saveFeature(feature2);

        assertThat(featureRepo.count()).isEqualTo(3);
    }

    @Test
    @Rollback(false)
    @Order(2)
    void testGetAll()
    {
        var names = new ArrayList<>(List.of("amogus", "amebus", "kenobus"));
        var features = featureService.getFeatures();

        var target = new ArrayList<Feature>();
        features.forEach(target::add);
        assertThat(target.size()).isEqualTo(3);
        for(var feature : features)
        {
            assertThat(names.contains(feature.getFeature())).isTrue();
            names.remove(feature.getFeature());
        }
    }

    @Test
    @Rollback(false)
    @Order(3)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testFindFeature()
    {
        var names = new ArrayList<>(List.of("amogus", "amebus", "kenobus"));

        for(var name : names)
            assertThat(featureService.findFeatureByName(name)).isNotNull();
    }
}
