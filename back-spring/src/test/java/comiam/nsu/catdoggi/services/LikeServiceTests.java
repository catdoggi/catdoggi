package comiam.nsu.catdoggi.services;

import comiam.nsu.catdoggi.config.ServiceTestConfig;
import comiam.nsu.catdoggi.models.*;
import comiam.nsu.catdoggi.models.repos.AnimalRepo;
import comiam.nsu.catdoggi.responces.AnimalPreviewResponse;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import(ServiceTestConfig.class)
class LikeServiceTests
{
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AnimalRepo animalRepo;

    @Autowired
    private UserService userService;

    @Autowired
    private LikeService likeService;

    @Autowired
    private AnimalService animalService;

    @Test
    @Rollback(false)
    @Order(1)
    void testUserNull() {
        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God5", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        assertThat(likeService.setLikeToAnimal(animal)).isFalse();
    }


    @Test
    @Rollback(false)
    @Order(2)
    void testAnimalLikeUserNull()
    {
        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("ki", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        Animal animal1 = animalService.getAnimalByID(2, true);
        assertThat(animal1.isLiked()).isFalse();
    }

    @Test
    @Rollback(false)
    @Order(3)
    void testAnimalLike()
    {
        User testUser = new User("lya", "nya");
        userService.setTestUser(testUser);

        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("Jerry", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        likeService.setLikeToAnimal(animal);
        Animal animal1 = animalService.getAnimalByID(3, true);
        assertThat(animal1.isLiked()).isTrue();
    }

    @Test
    @Rollback(false)
    @Order(4)
    void testLike()
    {
        User testUser = new User("lya", "lya");
        userService.setTestUser(testUser);
        User user = userService.getLoggedInUser();

        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God0", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        likeService.setLikeToAnimal(animal);
        List<Like> likes = likeService.getLikesOfCurrentUser(user);
        assertThat(likes.get(0).getAnimal().getName()).isEqualTo("God0");
    }



    @Test
    @Rollback(false)
    @Order(5)
    void testZero() {
        User testUser = new User("lya", "lya1");
        userService.setTestUser(testUser);
        User user = userService.getLoggedInUser();

        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God1", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        List<Like> likes = likeService.getLikesOfCurrentUser(user);
        assertThat(likes.size()).isZero();
    }

    @Test
    @Rollback(false)
    @Order(6)
    void testIsLikedTrue() {
        User testUser = new User("lya", "lya2");
        userService.setTestUser(testUser);
        User user = userService.getLoggedInUser();

        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God2", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        likeService.setLikeToAnimal(animal);
        assertThat(likeService.userLikedThatAnimal(user, animal)).isTrue();
    }

    @Test
    @Rollback(false)
    @Order(7)
    void testIsLikedFalse() {
        User testUser = new User("lya", "lya3");
        userService.setTestUser(testUser);
        User user = userService.getLoggedInUser();

        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God3", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        assertThat(likeService.userLikedThatAnimal(user, animal)).isFalse();
    }

    @Test
    @Rollback(false)
    @Order(8)
    void testAnimalByUser() {
        User testUser = new User("lya", "lya4");
        userService.setTestUser(testUser);

        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God4", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        likeService.setLikeToAnimal(animal);
        var animals = (ArrayList <AnimalPreviewResponse>)animalService.getFavorites();
        assertThat(animals.get(0).getType()).isEqualTo(AnimalType.DOG);

        userService.setTestUser(null);
        animals = (ArrayList <AnimalPreviewResponse>)animalService.getFavorites();
        assertThat(animals).isEmpty();
    }

    @Test
    @Rollback(false)
    @Order(9)
    void testUnlike()
    {
        User testUser = new User("lya", "lya6");
        userService.setTestUser(testUser);
        User user = userService.getLoggedInUser();

        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God6", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animalRepo.save(animal);

        likeService.setLikeToAnimal(animal);
        likeService.setLikeToAnimal(animal);
        List<Like> likes = likeService.getLikesOfCurrentUser(user);
        assertThat(likes.size()).isZero();
    }

    @Test
    @Rollback(false)
    @Order(10)
    void testAnimalLikeNull()
    {
        User testUser = new User("lya", "mir");
        userService.setTestUser(testUser);

        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("mi", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animal = animalRepo.save(animal);

        Animal animal1 = animalService.getAnimalByID(animal.getId(), true);
        assertThat(animal1.isLiked()).isFalse();
    }

    @Test
    @Rollback(false)
    @Order(11)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testLikeNullAnimal()
    {
        assertThat(likeService.setLikeToAnimal(null)).isFalse();
    }
}
