package comiam.nsu.catdoggi.services;

import comiam.nsu.catdoggi.config.ServiceTestConfig;
import comiam.nsu.catdoggi.models.Role;
import comiam.nsu.catdoggi.models.User;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import(ServiceTestConfig.class)
class UserServiceTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserService userService;

    @Test
    @Rollback(false)
    @Order(1)
    void testUserNull()
    {
        Role role = new Role(1L, "ROLE_USER");
        entityManager.persist(role);
        User user = new User("anohana", "menma");
        Set <Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        entityManager.persist(user);

        User userLoad = (User) userService.loadUserByUsername("menma");
        assertThat(userLoad.getUsername()).isEqualTo("menma");
    }

    @Test
    @Rollback(false)
    @Order(2)
    void testLoadById()
    {
        User userLoad = userService.loadUserById(1);
        assertThat(userLoad.getUsername()).isEqualTo("menma");
    }

    @Test
    @Rollback(false)
    @Order(3)
    void testSaveUser()
    {
        Role role = new Role(1L, "ROLE_USER");
        Set <Role> roles = new HashSet<>();
        roles.add(role);
        User user = new User("kpp", "gwen");
        user.setRoles(roles);

        userService.saveUser(user);

        User userLoad = (User)userService.loadUserByUsername("gwen");
        assertThat(userLoad.getUsername()).isEqualTo("gwen");

        var res = userService.saveUser(user);
        assertThat(res).isFalse();
    }

    @Test
    @Rollback(false)
    @Order(4)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testFindByUsernameAndPassword()
    {
        assertThat(userService.findByUsernameAndPassword("gwen", "kpp")).isNotNull();
    }

    @Test
    @Order(5)
    void testNegativeLoadUserByUsername()
    {
        boolean result = false;
        try
        {
            userService.loadUserByUsername("wefhweivunwie");
        }catch (UsernameNotFoundException e) {
            result = true;
        }

        assertThat(result).isTrue();
    }

    @Test
    @Order(6)
    void testNegativeLoadUserByID()
    {
        boolean result = false;
        try
        {
            userService.loadUserById(124);
        }catch (UsernameNotFoundException e) {
            result = true;
        }

        assertThat(result).isTrue();
    }

    @Test
    @Order(7)
    void testNegativeFindByUsernameAndPassword()
    {
        var result = userService.findByUsernameAndPassword("efef", "dwa");
        assertThat(result).isNull();
    }

    @Test
    @Order(8)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testNotAuth()
    {
        assertThat(userService.getLoggedInUser()).isNull();
    }
}
