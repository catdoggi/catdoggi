package comiam.nsu.catdoggi.services;

import comiam.nsu.catdoggi.config.ServiceTestConfig;
import comiam.nsu.catdoggi.models.*;
import comiam.nsu.catdoggi.models.repos.AnimalRepo;
import comiam.nsu.catdoggi.models.repos.FeatureRepo;
import comiam.nsu.catdoggi.request.AnimalData;
import comiam.nsu.catdoggi.responces.AnimalPreviewResponse;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import(ServiceTestConfig.class)
class AnimalServiceTests
{
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AnimalRepo animalRepo;

    @Autowired
    private FeatureRepo featureRepo;

    @Autowired
    private AnimalService animalService;

    @Test
    @Rollback(false)
    @Order(1)
    void testGetAll()
    {
        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God0", AnimalType.DOG, "url", "80", "80", health, care, beh);
        entityManager.persist(animal);

        beh = new BehaviourDescription(0, "beh1");
        health = new HealthDescription(0, "health1");
        care = new CareDescription(0, "care1");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        animal = new Animal("God1", AnimalType.DOG, "url", "80", "80", health, care, beh);
        entityManager.persist(animal);

        beh = new BehaviourDescription(0, "beh2");
        health = new HealthDescription(0, "health2");
        care = new CareDescription(0, "care2");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        animal = new Animal("God2", AnimalType.CAT, "url", "80", "80", health, care, beh);
        entityManager.persist(animal);

        beh = new BehaviourDescription(0, "beh3");
        health = new HealthDescription(0, "health3");
        care = new CareDescription(0, "care3");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        animal = new Animal("God3", AnimalType.CAT, "url", "80", "80", health, care, beh);
        entityManager.persist(animal);

        var animals = animalRepo.findAll();
        List<Animal> target = new ArrayList<>(animals);

        assertThat(target.size()).isEqualTo(4);
    }

    @Test
    @Rollback(false)
    @Order(2)
    void testGetDogs()
    {
        var dogs = animalService.getDogs();

        boolean check = true;
        for (var i : dogs)
            if (i.getType() != AnimalType.DOG)
            {
                check = false;
                break;
            }

        List<AnimalPreviewResponse> target = new ArrayList<>();
        dogs.forEach(target::add);

        assertThat(check).isTrue();
        assertThat(target.size()).isEqualTo(2);
    }

    @Test
    @Rollback(false)
    @Order(3)
    void testGetCats()
    {
        var cats = animalService.getCats();

        boolean check = true;
        for (var i : cats)
            if (i.getType() != AnimalType.CAT)
            {
                check = false;
                break;
            }

        List<AnimalPreviewResponse> target = new ArrayList<>();
        cats.forEach(target::add);

        assertThat(check).isTrue();
        assertThat(target.size()).isEqualTo(2);
    }

    @Test
    @Rollback(false)
    @Order(4)
    void testGetAnimal()
    {
        long size = animalRepo.count();
        for (int i = 1; i < size + 1; i++)
        {
            var animal = animalService.getAnimalByID(i, false);

            var beh = new BehaviourDescription(i, "beh" + (i - 1));
            var health = new HealthDescription(i, "health" + (i - 1));
            var care = new CareDescription(i, "care" + (i - 1));

            var animal1 = new Animal("God" + (i - 1), i > 2 ? AnimalType.CAT : AnimalType.DOG, "url", "80", "80", health, care, beh);
            animal1.setId(i);

            assertThat(animal.toString()).hasToString(animal1.toString());
            System.out.println(i + "th object scanned successfully");
        }
    }

    @Test
    @Rollback(false)
    @Order(5)
    void testDeleteAll()
    {
        var animals = animalRepo.findAll();

        boolean successfullyDeleted = true;
        for (var animal : animals)
            if (!animalService.deleteAnimal(animal.getId()))
            {
                successfullyDeleted = false;
                break;
            }
        assertThat(successfullyDeleted).isTrue();
        assertThat(animalRepo.count()).isZero();
    }

    @Test
    @Rollback(false)
    @Order(6)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testCreateNew()
    {
        var features = new ArrayList<String>();
        features.add("плоти");
        features.add("нологи");
        var data = new AnimalData("a", false, AnimalType.CAT, "a", "a", "a", "a", "a", "a", features);

        animalService.saveAnimal(data);
        assertThat(featureRepo.count()).isEqualTo(2);
        assertThat(animalRepo.findAll().get(0).getName()).isEqualTo("a");

        data = new AnimalData("a24", false, AnimalType.CAT, "a", "a", "a", "a", "a", "a", features);
        animalService.saveAnimal(data);
        assertThat(featureRepo.count()).isEqualTo(2);
        assertThat(animalRepo.count()).isEqualTo(2);
    }

    @Test
    @Rollback(false)
    @Order(7)
    void testFalseDeletingAnimal()
    {
        var res = animalService.deleteAnimal(0);

        assertThat(res).isFalse();
    }

    @Test
    @Rollback(false)
    @Order(8)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testSimplifyAnimalEntity()
    {
        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care0");

        var animal = new Animal("God0", AnimalType.DOG, "url", "80", "80", health, care, beh);

        var simplified = animalService.simplifyAnimalEntity(animal);

        assertThat(simplified.getAverageAge()).hasToString(animal.getAverageAge());
        assertThat(simplified.getAverageWeight()).hasToString(animal.getAverageWeight());
        assertThat(simplified.getType()).isEqualTo(animal.getType());
        assertThat(simplified.getBehaviourDescription()).hasToString(animal.getBehaviourDescription().getName());
        assertThat(simplified.getCareDescription()).hasToString(animal.getCareDescription().getName());
        assertThat(simplified.getHealthDescription()).hasToString(animal.getHealthDescription().getName());
        assertThat(simplified.getUrl()).hasToString(animal.getUrl());
        assertThat(simplified.getName()).hasToString(animal.getName());
        assertThat(animalService.simplifyAnimalEntity(null)).isNull();
    }

    @Test
    @Rollback(false)
    @Order(9)
    void testCount()
    {
        BehaviourDescription behaviourDescription = new BehaviourDescription(0, "behMua");
        HealthDescription healthDescription = new HealthDescription(0, "healthMua");
        CareDescription careDescription = new CareDescription(0, "careMua");

        entityManager.persist(behaviourDescription);
        entityManager.persist(healthDescription);
        entityManager.persist(careDescription);

        Animal animalLuna = new Animal("Luna", AnimalType.CAT, "https://sailor", "10", "10",
                healthDescription, careDescription, behaviourDescription);
        entityManager.persist(animalLuna);

        Feature feature = new Feature(0, "moon", null);
        Set<Animal> set = new HashSet<>();
        set.add(animalLuna);
        feature.setAnimals(set);
        entityManager.persist(feature);

        List<AnimalPreviewResponse> res =
                (List<AnimalPreviewResponse>) animalService.findAnimalsByFeatures("moon", "CAT");
        assertThat(res.size()).isOne();
    }

    @Test
    @Rollback(false)
    @Order(10)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testOrder()
    {
        BehaviourDescription behaviourDescription = new BehaviourDescription(0, "behLya");
        HealthDescription healthDescription = new HealthDescription(0, "healthLya");
        CareDescription careDescription = new CareDescription(0, "careLya");

        entityManager.persist(behaviourDescription);
        entityManager.persist(healthDescription);
        entityManager.persist(careDescription);

        Animal animalHappy = new Animal("Happy", AnimalType.CAT, "https://happyyyy",
                "10", "10", healthDescription, careDescription, behaviourDescription);
        entityManager.persist(animalHappy);

        behaviourDescription = new BehaviourDescription(0, "behNya");
        healthDescription = new HealthDescription(0, "healthNya");
        careDescription = new CareDescription(0, "careNya");

        entityManager.persist(behaviourDescription);
        entityManager.persist(healthDescription);
        entityManager.persist(careDescription);

        Animal animalBond = new Animal("Bond", AnimalType.DOG, "https://spyfamily", "10", "10",
                healthDescription, careDescription, behaviourDescription);
        entityManager.persist(animalBond);

        behaviourDescription = new BehaviourDescription(0, "behMya");
        healthDescription = new HealthDescription(0, "healthMya");
        careDescription = new CareDescription(0, "careMya");

        entityManager.persist(behaviourDescription);
        entityManager.persist(healthDescription);
        entityManager.persist(careDescription);

        Animal animalSharly = new Animal("Sharly", AnimalType.CAT, "https://wendy", "10", "10",
                healthDescription, careDescription, behaviourDescription);
        entityManager.persist(animalSharly);

        Feature feature = new Feature(0, "fly", null);
        Set<Animal> set = new HashSet<>();
        set.add(animalHappy);
        set.add(animalSharly);
        feature.setAnimals(set);
        entityManager.persist(feature);

        feature = new Feature(0, "future", null);
        set = new HashSet<>();
        set.add(animalBond);
        set.add(animalSharly);
        feature.setAnimals(set);
        entityManager.persist(feature);

        List<AnimalPreviewResponse> res =
                (List<AnimalPreviewResponse>) animalService.findAnimalsByFeatures("fly, future", "DOG");
        assertThat(res.size()).isZero();
    }
}
