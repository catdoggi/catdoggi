package comiam.nsu.catdoggi.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@TestConfiguration
@EnableJpaRepositories(basePackages = "comiam.nsu.catdoggi.models.repos")
@EntityScan(basePackages = "comiam.nsu.catdoggi.models")
@ComponentScan(basePackages = "comiam.nsu.catdoggi", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE))
@EnableAutoConfiguration
public class ServiceTestConfig
{

}
