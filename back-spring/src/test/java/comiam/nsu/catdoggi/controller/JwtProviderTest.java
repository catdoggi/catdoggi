package comiam.nsu.catdoggi.controller;

import comiam.nsu.catdoggi.config.ServiceTestConfig;
import comiam.nsu.catdoggi.jwt.JwtProvider;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import(ServiceTestConfig.class)
class JwtProviderTest
{
    @Autowired
    JwtProvider jwtProvider;

    @Test
    void testGenerateToken()
    {
        var token = jwtProvider.generateToken("0");
        assertThat(token).isNotNull();
    }

    @Test
    void testValidateToken()
    {
        var token = jwtProvider.generateToken("0");
        var result = jwtProvider.validateToken(token);

        assertThat(result).isTrue();
    }

    @Test
    void testGetIdFromToken()
    {
        var token = jwtProvider.generateToken("42");
        var resId = jwtProvider.getIdFromToken(token);

        assertThat(resId).hasToString("42");
    }

    @Test
    void testValidateTokenError()
    {
        var resId = jwtProvider.validateToken("awd");

        assertThat(resId).isFalse();
    }
}
