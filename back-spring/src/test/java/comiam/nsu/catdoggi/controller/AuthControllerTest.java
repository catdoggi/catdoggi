package comiam.nsu.catdoggi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import comiam.nsu.catdoggi.CatDoggiApplication;
import comiam.nsu.catdoggi.jwt.JwtProvider;
import comiam.nsu.catdoggi.models.Role;
import comiam.nsu.catdoggi.models.User;
import comiam.nsu.catdoggi.models.repos.RoleRepo;
import comiam.nsu.catdoggi.models.repos.UsersRepo;
import comiam.nsu.catdoggi.request.LoginRequest;
import comiam.nsu.catdoggi.request.RegistrationRequest;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(classes = {CatDoggiApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class AuthControllerTest
{
    @Autowired
    private WebApplicationContext context;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private UsersRepo usersRepo;
    @Autowired
    private JwtProvider jwtProvider;

    private MockMvc mockMvc;

    @BeforeEach
    void initMock()
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    @Order(1)
    void testSignInAdmin() throws Exception
    {
        Role role;
        role = new Role(1L, "ROLE_ADMIN");
        roleRepo.save(role);

        var admin = new User("dwa", "admin2");
        admin.setRoles(Collections.singleton(role));
        admin.setPassword(bCryptPasswordEncoder.encode(admin.getPassword()));
        admin.setId(null);
        usersRepo.save(admin);

        LoginRequest loginRequest = new LoginRequest("admin2", "dwa");

        var res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk()).andReturn();

        var jwt = res.getResponse().getContentAsString();
        String token = jwt.split("\"")[3];

        assertThat(token).isNotEmpty();
        assertThat(usersRepo.count()).isPositive();
        assertThat(jwtProvider.validateToken(token)).isTrue();
    }

    @Test
    @Order(2)
    void testSignUpUser() throws Exception
    {
        RegistrationRequest reg = new RegistrationRequest("usr2", "pwd", "pwd");
        var json = mapper.writeValueAsString(reg);
        mockMvc.perform(post("/signup").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(status().isOk());
    }

    @Test
    @Order(3)
    void testSignInUsers() throws Exception
    {
        LoginRequest loginRequest = new LoginRequest("usr2", "pwd");

        var res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk()).andReturn();
        var jwt = res.getResponse().getContentAsString();

        String token = jwt.split("\"")[3];

        assertThat(jwtProvider.validateToken(token)).isTrue();

        loginRequest = new LoginRequest("admin2", "dwa");

        res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk()).andReturn();

        jwt = res.getResponse().getContentAsString();
        token = jwt.split("\"")[3];

        assertThat(jwtProvider.validateToken(token)).isTrue();
    }

    @Test
    @Order(4)
    void testSignUpRedirect() throws Exception
    {
        var res = mockMvc.perform(get("/signup")).andExpect(status().isOk()).andReturn();
        assertThat(res.getResponse().getContentAsString()).hasToString("signup");
    }

    @Test
    @Order(5)
    void testSignInRedirect() throws Exception
    {
        var res = mockMvc.perform(get("/signin")).andExpect(status().isOk()).andReturn();
        assertThat(res.getResponse().getContentAsString()).hasToString("signin");
    }

    @Test
    @Order(6)
    void testSignUpErrors() throws Exception
    {
        RegistrationRequest reg = new RegistrationRequest("usr2", null, "pwd");
        var json = mapper.writeValueAsString(reg);
        var res = mockMvc.perform(post("/signup").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(status().isBadRequest()).andReturn();

        assertThat(res.getResponse().getContentAsString()).contains("Error: password is empty");

        reg = new RegistrationRequest("usr2", "wef", "pwd");
        json = mapper.writeValueAsString(reg);
        res = mockMvc.perform(post("/signup").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(status().isBadRequest()).andReturn();

        assertThat(res.getResponse().getContentAsString()).contains("Error: password mismatch");

        reg = new RegistrationRequest("usr2", "wef", "wef");
        json = mapper.writeValueAsString(reg);
        res = mockMvc.perform(post("/signup").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(status().isBadRequest()).andReturn();

        assertThat(res.getResponse().getContentAsString()).contains("Error: ser with the same name already exists");
    }

    @Test
    @Order(7)
    void testSignInErrors() throws Exception
    {
        LoginRequest loginRequest = new LoginRequest("sefsef", "pwd");

        var res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isBadRequest()).andReturn();

        assertThat(res.getResponse().getContentAsString()).contains("Error");

        loginRequest = new LoginRequest("usr2", "wdwdwdwd");

        res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isBadRequest()).andReturn();

        assertThat(res.getResponse().getContentAsString()).contains("Error");

        loginRequest = new LoginRequest("easfse", "wdwdwdwd");

        res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isBadRequest()).andReturn();

        assertThat(res.getResponse().getContentAsString()).contains("Error");
    }
}
