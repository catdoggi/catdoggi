package comiam.nsu.catdoggi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import comiam.nsu.catdoggi.CatDoggiApplication;
import comiam.nsu.catdoggi.models.AnimalType;
import comiam.nsu.catdoggi.models.Role;
import comiam.nsu.catdoggi.models.User;
import comiam.nsu.catdoggi.models.repos.AnimalRepo;
import comiam.nsu.catdoggi.models.repos.LikeRepo;
import comiam.nsu.catdoggi.models.repos.RoleRepo;
import comiam.nsu.catdoggi.models.repos.UsersRepo;
import comiam.nsu.catdoggi.request.AnimalData;
import comiam.nsu.catdoggi.request.LoginRequest;
import comiam.nsu.catdoggi.request.RegistrationRequest;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(classes = {CatDoggiApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class SessionControllerTest
{
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private UsersRepo usersRepo;
    @Autowired
    private AnimalRepo animalRepo;
    @Autowired
    private LikeRepo likeRepo;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    ObjectMapper mapper;

    private MockMvc mockMvc;

    @BeforeEach
    void initMock()
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    @Order(1)
    void testAddAnimal() throws Exception
    {
        Role role;
        role = new Role(2L, "ROLE_ADMIN");
        roleRepo.save(role);

        var admin = new User("dwa", "admin");
        admin.setRoles(Collections.singleton(role));
        admin.setPassword(bCryptPasswordEncoder.encode(admin.getPassword()));
        admin.setId(null);
        usersRepo.save(admin);

        mockMvc.perform(get("/users/role")).andExpect(status().isOk())
                .andExpect(jsonPath("$.admin").value("false"));

        LoginRequest loginRequest = new LoginRequest("admin", "dwa");

        var res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk()).andReturn();

        var jwt = res.getResponse().getContentAsString();
        String token = jwt.split("\"")[3];

        mockMvc.perform(get("/users/role").header("Authorization", "Bearer " + token)).andExpect(status().isOk())
                .andExpect(jsonPath("$.admin").value("true"));

        var features = new ArrayList<String>();
        features.add("amogus");
        features.add("amebus");
        var data = new AnimalData("God0", false, AnimalType.DOG, "a", "a", "a",
                "a", "a", "a", features);

        mockMvc.perform(post("/admin/animal/add").header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(data)))
                .andExpect(status().isOk());

        features = new ArrayList<>();
        features.add("kenobus");

        data = new AnimalData("God2", false, AnimalType.CAT, "a", "a", "a",
                "a", "a", "a", features);

        mockMvc.perform(post("/admin/animal/add").header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(data)))
                .andExpect(status().isOk());

        assertThat(animalRepo.count()).isEqualTo(2);
    }

    @Test
    @Order(2)
    void testGetAnimals() throws Exception
    {
        mockMvc.perform(get("/animals/dogs")).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.equalTo("God0")));
        mockMvc.perform(get("/animals/cats")).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.equalTo("God2")));
    }

    @Test
    @Order(3)
    void testGetAnimalByID() throws Exception
    {
        mockMvc.perform(get("/animals/1")).andExpect(status().isOk()).andExpect(jsonPath("$.name").value("God0"));
        mockMvc.perform(get("/animals/2")).andExpect(status().isOk()).andExpect(jsonPath("$.name").value("God2"));
    }

    @Test
    @Order(4)
    void testGetFeatures() throws Exception
    {
        mockMvc.perform(get("/animals/features")).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(3)));
    }

    @Test
    @Order(5)
    void testFindAnimalByFeatureName() throws Exception
    {
        mockMvc.perform(get("/animals/search?features=kenobus&type=CAT"))
                .andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name").value("God2"));

        mockMvc.perform(get("/animals/search?features=amebus&type=DOG"))
                .andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name").value("God0"));

        mockMvc.perform(get("/animals/search?features=amogus&type=DOG"))
                .andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name").value("God0"));
    }

    @Test
    @Order(6)
    void testSetLikeToAnimal() throws Exception
    {
        RegistrationRequest reg = new RegistrationRequest("usr", "pwd", "pwd");
        var json = mapper.writeValueAsString(reg);
        mockMvc.perform(post("/signup").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(status().isOk());

        LoginRequest loginRequest = new LoginRequest("usr", "pwd");

        var res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk()).andReturn();
        var jwt = res.getResponse().getContentAsString();

        String token = jwt.split("\"")[3];

        mockMvc.perform(post("/animals/like/1").header("Authorization", "Bearer " + token)).andExpect(status().isOk());
        mockMvc.perform(get("/animals/favorites").header("Authorization", "Bearer " + token)).andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.equalTo("God0")));

        mockMvc.perform(post("/animals/like/2").header("Authorization", "Bearer " + token)).andExpect(status().isOk());
        mockMvc.perform(get("/animals/favorites").header("Authorization", "Bearer " + token)).andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].name", Matchers.equalTo("God0")))
                .andExpect(jsonPath("$[1].name", Matchers.equalTo("God2")));

        mockMvc.perform(get("/animals/dogs").header("Authorization", "Bearer " + token))
                .andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.equalTo("God0")))
                .andExpect(jsonPath("$[0].likedByCurrentUser", Matchers.equalTo(true)));

        mockMvc.perform(get("/animals/cats").header("Authorization", "Bearer " + token))
                .andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.equalTo("God2")))
                .andExpect(jsonPath("$[0].likedByCurrentUser", Matchers.equalTo(true)));

        mockMvc.perform(get("/animals/1").header("Authorization", "Bearer " + token))
                .andExpect(status().isOk()).andExpect(jsonPath("$.name").value("God0"))
                .andExpect(status().isOk()).andExpect(jsonPath("$.liked").value(true));

        mockMvc.perform(get("/animals/2").header("Authorization", "Bearer " + token))
                .andExpect(status().isOk()).andExpect(jsonPath("$.name").value("God2"))
                .andExpect(status().isOk()).andExpect(jsonPath("$.liked").value(true));

        mockMvc.perform(post("/animals/like/1").header("Authorization", "Bearer " + token)).andExpect(status().isOk());
        mockMvc.perform(get("/animals/favorites").header("Authorization", "Bearer " + token)).andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.equalTo("God2")));

        mockMvc.perform(post("/animals/like/2").header("Authorization", "Bearer " + token)).andExpect(status().isOk());
        mockMvc.perform(get("/animals/favorites").header("Authorization", "Bearer " + token)).andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(0)));

        mockMvc.perform(post("/animals/like/200").header("Authorization", "Bearer " + token)).andExpect(status().isBadRequest());

        assertThat(likeRepo.count()).isZero();
    }

    @Test
    @Order(7)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testRemoveAnimal() throws Exception
    {
        LoginRequest loginRequest = new LoginRequest("admin", "dwa");

        var res = mockMvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk()).andReturn();

        var jwt = res.getResponse().getContentAsString();
        String token = jwt.split("\"")[3];

        mockMvc.perform(post("/admin/animal/remove/1")
                .header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        mockMvc.perform(post("/admin/animal/remove/2")
                .header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        mockMvc.perform(post("/admin/animal/remove/200")
                .header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        assertThat(animalRepo.count()).isZero();
        assertThat(usersRepo.count()).isPositive();
        assertThat(likeRepo.count()).isZero();
    }
}