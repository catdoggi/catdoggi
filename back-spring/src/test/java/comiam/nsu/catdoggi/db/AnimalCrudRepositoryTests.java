package comiam.nsu.catdoggi.db;

import comiam.nsu.catdoggi.config.ServiceTestConfig;
import comiam.nsu.catdoggi.models.*;
import comiam.nsu.catdoggi.models.repos.AnimalRepo;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import(ServiceTestConfig.class)
class AnimalCrudRepositoryTests
{
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AnimalRepo repository;

    @Test
    @Rollback(false)
    @Order(1)
    void testSaveAnimal()
    {
        var beh = new BehaviourDescription(0, "beh0");
        var health = new HealthDescription(0, "health0");
        var care = new CareDescription(0, "care");

        entityManager.persist(beh);
        entityManager.persist(health);
        entityManager.persist(care);

        var animal = new Animal("God", AnimalType.DOG, "url", "80", "80", health, care, beh);

        entityManager.persist(animal);
        assertThat(repository.count()).isPositive();
    }

    @Test
    @Rollback(false)
    @Order(2)
    void testReadAnimal()
    {
        Animal testAnimal = repository.findById(1).get();

        var beh = new BehaviourDescription(1, "beh0");
        var health = new HealthDescription(1, "health0");
        var care = new CareDescription(1, "care");

        var animal = new Animal("God", AnimalType.DOG, "url", "80", "80", health, care, beh);
        animal.setId(1);

        assertThat(testAnimal.toString()).hasToString(animal.toString());
    }

    @Test
    @Rollback(false)
    @Order(3)
    void testUpdateAnimal()
    {
        Animal testAnimal = repository.findById(1).get();
        testAnimal.setAverageAge("90");

        repository.save(testAnimal);
        testAnimal = repository.findById(1).get();

        assertThat(testAnimal.getAverageAge()).isEqualTo("90");
    }

    @Test
    @Rollback(false)
    @Order(4)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void testDeleteAnimal()
    {
        repository.deleteById(1);

        var testAnimal = repository.findById(1);

        assertThat(testAnimal).isNotPresent();
    }
}
