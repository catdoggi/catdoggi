import React, { useState, useEffect } from 'react';
import SignInPage from './components/authPages/SignInPage';
import RegistrationPage from './components/authPages/RegistrationPage'
import NMenu from './components/NavigateMenu/NMenu';
import PetsView from './components/Pets/PetsView';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import PetDetails from './components/Pets/Pet/PetDetails/PetDetails';
import LogOut from './components/authPages/LogOut';
import Favorites from './components/Pets/Favorites/Favorites';
import axios from 'axios';
import AddPet from './components/Pets/AddPet/AddPet';

const App = () => {
    const [pets, setPets] = useState([]);
    const [mode, setMode] = useState({});
    const [options, setOptions] = useState([]);
   
    const fetchCats = async () => {
        setMode("cats")
        console.log("\n fetchCats token: Bearer " + sessionStorage.getItem('myTokenName'));
        axios({
          method: 'GET',
          url: 'https://cat-doggi-back.herokuapp.com/animals/cats',
          headers: {
            'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
          },       
        })

        .then(response => response.data)
        .then(data => setPets(data))
        .catch(error => {
            console.log(error)
        })
    };

    const fetchDogs = async () => {
        setMode("dogs")
        console.log("\n fetchDogs token: Bearer " + sessionStorage.getItem('myTokenName'));
        axios({
          method: 'GET',
          url: 'https://cat-doggi-back.herokuapp.com/animals/dogs',
          headers: {
            'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
          },       
        })
        .then(response => response.data)
        .then(data => setPets(data))
        .catch(error => {
            console.log(error)
        })
    };

    const fetchOptions = async () => {
        axios({
            method: 'GET',
            url: 'https://cat-doggi-back.herokuapp.com/animals/features',
            headers: {
              'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
            },       
        })
        .then(response => response.data)
        .then(data => setOptions(data))
        .catch(error => {
            console.log(error)
        })
    };

    const handleClickOnLiked = async () => {
        console.log("\nhandleClickOnLiked sending token: Bearer " + sessionStorage.getItem('myTokenName'));
        axios({
            method: 'GET',
            url: 'https://cat-doggi-back.herokuapp.com/animals/favorites',
            headers: {
              'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
            },       
        })
        .then(response => setPets(response.data))
        .then(setMode("favs"))
        .catch(error => console.log(error));
    };

    const didSelectFeature = async (feature) => {
        const modeToSend = (mode === "cats" ? "CAT" : "DOG");
        console.log("\ndidSelectFeature asking with options " + JSON.stringify("features : " + feature));
        axios.get('https://cat-doggi-back.herokuapp.com/animals/search', {
            params: {
                features: feature,
                type: modeToSend
            }
        })
        .then(response => setPets(response.data))
        .catch(error => console.log(error));

    }

    function handleClickOnFav() {
        update()
    };

    function update() {
        fetchOptions()
        console.log("will update with mode = " + mode)
        if (mode === "cats") {
            fetchCats()
        } else if (mode === "dogs") {
            fetchDogs()
        } else {
            handleClickOnLiked()
        }
        console.log("did update with mode = " + mode)
    }

    function changeMode(newMode) {
        if (newMode === "cats") {
            setMode("cats")
        } else if (newMode === "dogs") {
            setMode("dogs")
        } else {
            setMode("favs")
        }
        console.log('newMode, mode = ' + newMode + ', ' + mode)
        update()
    }

    useEffect(() => {
        console.log('app use effect')
        fetchCats()
        fetchOptions()
        }, []);

    return (
        <Router>
            <div>
                <CssBaseline />
                <NMenu/>
                <Switch>
                    <Route exact path="/">
                        <PetsView pets={pets} mode="cats" options={options} changeMode={changeMode}
                                  didClickOnFav={handleClickOnFav}  didClickCats={fetchCats} didClickDogs={fetchDogs} 
                                  didSelectFeature={didSelectFeature} deleteFeature={update}/>
                    </Route>
                    <Route exact path="/cats">
                        <PetsView pets={pets} mode="cats" options={options} changeMode={changeMode} didClickOnFav={handleClickOnFav}  
                        didClickCats={fetchCats} didClickDogs={fetchDogs} 
                        didSelectFeature={didSelectFeature} deleteFeature={update}/>
                    </Route>
                    <Route exact path="/dogs">
                        <PetsView pets={pets} mode="dogs" options={options} changeMode={changeMode} didClickOnFav={handleClickOnFav}  
                        didClickCats={fetchCats} didClickDogs={fetchDogs} 
                        didSelectFeature={didSelectFeature} deleteFeature={update}/>
                    </Route>
                    <Route exact path="/favorites">
                        <Favorites pets={pets} didClickOnFav={handleClickOnFav} didClickOnLiked={handleClickOnLiked}/>
                    </Route>
                    <Route exact path="/signin">
                        <SignInPage/>
                    </Route>
                    <Route exact path="/signup">
                        <RegistrationPage/>
                    </Route>
                    <Route exact path="/logout">
                        <LogOut/>
                    </Route>
                    <Route exact path="/addPet">
                        <AddPet/>
                    </Route>
                    <Route path="/pet/:id" component={Info}/>
                </Switch>
            </div>
        </Router>
    )
}

export default App;

class Info extends React.Component{
    render(){
        // получаем параметры
        const id = this.props.match.params.id;
        return <PetDetails animalId={id}/>
    }
}