import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {AppBar, Toolbar, IconButton, MenuItem} from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreIcon from '@material-ui/icons/MoreVert';
import { Favorite, Home } from '@material-ui/icons';
import { Link, Redirect } from 'react-router-dom';
import logo from '../../logo.png';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  logo: {
    maxHeight: "80px",
    marginLeft: -10
  },
}));

export default function PrimarySearchAppBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [isAdmin, setAdmin] = useState({});

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    checkAdmin(sessionStorage.getItem('myTokenName'));
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const checkAdmin = (token) => {
    axios({
        method: 'GET',
        url: 'https://cat-doggi-back.herokuapp.com/users/role',
        headers: {
          'Authorization': "Bearer " + token,
        }
      })
      .then(response => {
        setAdmin(response.data.admin);
      })
      .catch(error => console.log(error))
  }

  const handleMobileMenuOpen = (event) => {
    checkAdmin(sessionStorage.getItem('myTokenName'));
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleLogOut = () => {
    sessionStorage.setItem('myTokenName', null);
    handleMenuClose();
  };

  const menuId = 'primary-search-account-menu';
  
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      {(isAdmin === "true" || isAdmin === true) &&
        <MenuItem component={Link} to="/addPet">Add pet</MenuItem>
      }
      {(sessionStorage.getItem('myTokenName') !== "null" && sessionStorage.getItem('myTokenName') !== null) &&
        <MenuItem component={Link} to="/logout" onClick={handleLogOut}>Log out</MenuItem>
      }
      {(sessionStorage.getItem('myTokenName') === "null" || sessionStorage.getItem('myTokenName') === null) &&
        <MenuItem component={Link} to="/signin" onClick={handleMenuClose}>Sign in</MenuItem>
      }
      {(sessionStorage.getItem('myTokenName') === "null" || sessionStorage.getItem('myTokenName') === null) &&
        <MenuItem component={Link} to="/signup" onClick={handleMenuClose}>Registration</MenuItem>
      }
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
       <MenuItem>
        <IconButton aria-label="favourites of current user" color="inherit" component={Link} to="/favorites">
          <Favorite />
        </IconButton>
        <p>Favorites</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="fixed" style={{ background: '#7B68EE' }}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
            component={Link} to="/"
          >
            <Home />
          </IconButton>
          <img src={logo} alt="logo" className={classes.logo} />
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton aria-label="favourites of current user" color="inherit" component={Link} to="/favorites">
              <Favorite />
            </IconButton>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
}

//export default NMenu;