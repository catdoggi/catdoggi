import React, {useState} from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {Redirect} from 'react-router-dom'

import axios from 'axios';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(12),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

const RegistrationPage = () => {
    const classes = useStyles();
    const [state, setState] = useState({
      username: "",
      password: "",
      confirmPassword: ""
    })

    const [isRegistred, setRegistred] = useState({});

    const handleRegistration = (event) => {
      event.preventDefault();
      console.log(JSON.stringify(state));
      axios({
        method: 'POST',
        url: 'https://cat-doggi-back.herokuapp.com/signup',
        headers: {
          'Content-Type': 'application/json',
        },
        data: JSON.stringify(state)        
      })
        .then(response => {
          console.log(response);
          setRegistred("true");
        })
        .catch(error => {
          console.log(error);
          setRegistred("false");
        })              
    }

    // write to storage
    //sessionStorage.setItem('myTokenName', token)

    // read from storage
    //sessionStorage.getItem('myTokenName')

    if (isRegistred === "true" || isRegistred === true){
      return <Redirect push to="/signin"/>
    }

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Registration
          </Typography>
          {(isRegistred === "false" || isRegistred === false) &&
          <h3 style={{ color: 'red' }}>Ошибка регистрации</h3>
          }
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="login"
              label="Login"
              name="Login"
              autoComplete="Login"
              autoFocus
              onChange={(event) => {setState({
                username: event.target.value,
                password: state.password,
                confirmPassword: state.confirmPassword
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(event) => {setState({
                username: state.username,
                password: event.target.value,
                confirmPassword: state.confirmPassword
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="confirmPassword"
              label="Confirm password"
              type="password"
              id="confirmPassword"
              autoComplete="current-password"
              onChange={(event) => {setState({
                username: state.username,
                password: state.password,
                confirmPassword: event.target.value
              })}}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={handleRegistration}
            >
              Registration
            </Button>
          </form>
        </div>
      </Container>
    );
}

export default RegistrationPage