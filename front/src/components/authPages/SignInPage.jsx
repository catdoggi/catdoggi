import React, {useState} from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Link, Redirect } from 'react-router-dom'
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(12),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

const SignInPage = () => {
    const classes = useStyles();
    const [state, setState] = useState({
      username: "",
      password: ""
    })
    const [loggedIn, setLogged] = useState({});

    const handleSignIn = (event) => {
      event.preventDefault();
      axios({
        method: 'POST',
        url: 'https://cat-doggi-back.herokuapp.com/signin',
        headers: {
          'Content-Type': 'application/json',
        },
        data: JSON.stringify(state)        
      })
        .then(response => {
          sessionStorage.setItem('myTokenName', response.data.token);
          setLogged("true");
        }) 
        .catch(error => {
          console.log(error);
          setLogged("false");
        });           
    }
    
    if (loggedIn === "true" || loggedIn === true){
      return <Redirect push to="/"/>
    }

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          {(loggedIn === "false" || loggedIn === false) &&
          <h3 style={{ color: 'red' }}>Ошибка авторизации</h3>
          }
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="login"
              label="Login"
              name="Login"
              autoComplete="Login"
              autoFocus
              onChange={(event) => {setState({
                username: event.target.value,
                password: state.password
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(event) => {setState({
                username: state.username,
                password: event.target.value
              })}}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={handleSignIn}
            >
              Sign In
            </Button>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              component={Link} to="/signup"
            >
              Registration
            </Button>
          </form>
        </div>
      </Container>
    );
}

export default SignInPage