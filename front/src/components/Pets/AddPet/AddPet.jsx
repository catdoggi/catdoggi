import React, {useState, useEffect} from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Redirect } from 'react-router-dom';
import { AddCircleRounded } from '@material-ui/icons';
import axios from 'axios';
import { Select,Menu,  MenuItem } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(12),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    add: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

const AddPet = () => {
    window.scrollTo(0, 0);

    const classes = useStyles();
    const [type, setType] = useState("CAT");

    const [state, setState] = useState({
      type: "", //
      name: "", //
      url: "",  //
      averageWeight: "",  //
      averageAge: "", //
      healthDescription: "",  //
      careDescription: "",  //
      behaviourDescription: "", //
      liked: "false",  //
      features: [""] //
    })
    const [isAdded, setAdded] = useState();

    const handleChangeType = (event) => {
      setType(event.target.value);
    }

    const handleFeauturesChange = (event) => {
      const userFeatureString = event.target.value;
      const userFeatureArray = userFeatureString.split(', ');
      setState({
        type: type,
                name: state.name,
                url: state.url,
                averageWeight: state.averageWeight,
                averageAge: state.averageAge,
                healthDescription: state.healthDescription,
                careDescription: state.careDescription,
                behaviourDescription: state.behaviourDescription,
                liked: "false",
                features: userFeatureArray
      })
    }

    const handleClickOnAdd = () => {
      console.log("\nstate to the end: type " + state.type + ", name " + state.name + ", url " + state.url +
      ", avW " + state.averageWeight + ", avA " + state.averageAge + ", healD " + state.healthDescription +
      ", careD " + state.careDescription + ", behD " + state.behaviourDescription + ", liked " + state.liked);
      console.log("\nstate to send: " + JSON.stringify(state));
      axios({
        method: 'POST',
        url: 'https://cat-doggi-back.herokuapp.com/admin/animal/add',
        headers: {
          'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
          'Content-Type': 'application/json',
        },
        data: JSON.stringify(state),       
      })
      .then(response => {
        console.log(response);
        setAdded("true");
      })
      .catch(error =>{
        console.log(error);
        setAdded("false");
      })
    }

    if (isAdded === "true" || isAdded === true){
       return <Redirect push to="/"/>
    }

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar} color="inherit">
              <AddCircleRounded/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Add pet
          </Typography>
          {(isAdded === "false" || isAdded === false) &&
          <h3 style={{ color: 'red' }}>Ошибка добавления</h3>
          }
          <form className={classes.form} noValidate>
            <select autoFocus onChange={handleChangeType}>
              <option>CAT</option>
              <option>DOG</option>
            </select>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="name"
              label="Название породы"
              name="name"
              autoComplete="Название породы"
              onChange={(event) => {setState({
                type: type,
                name: event.target.value,
                url: state.url,
                averageWeight: state.averageWeight,
                averageAge: state.averageAge,
                healthDescription: state.healthDescription,
                careDescription: state.careDescription,
                behaviourDescription: state.behaviourDescription,
                liked: "false",
                features: [""]
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="url"
              label="URL картинки"
              name="url"
              autoComplete="URL картинки"
              onChange={(event) => {setState({
                type: type,
                name: state.name,
                url: event.target.value,
                averageWeight: state.averageWeight,
                averageAge: state.averageAge,
                healthDescription: state.healthDescription,
                careDescription: state.careDescription,
                behaviourDescription: state.behaviourDescription,
                liked: "false",
                features: [""]
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="avgW"
              label="Средний вес"
              name="avgW"
              autoComplete="Средний вес"
              onChange={(event) => {setState({
                type: type,
                name: state.name,
                url: state.url,
                averageWeight: event.target.value,
                averageAge: state.averageAge,
                healthDescription: state.healthDescription,
                careDescription: state.careDescription,
                behaviourDescription: state.behaviourDescription,
                liked: "false",
                features: [""]
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="avgA"
              label="Средняя продолжительность жизни"
              name="avgA"
              autoComplete="Средняя продолжительность жизни"
              onChange={(event) => {setState({
                type: type,
                name: state.name,
                url: state.url,
                averageWeight: state.averageWeight,
                averageAge: event.target.value,
                healthDescription: state.healthDescription,
                careDescription: state.careDescription,
                behaviourDescription: state.behaviourDescription,
                liked: "false",
                features: [""]
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="healthD"
              label="Описание особенностей здоровья"
              name="healthD"
              autoComplete="Описание особенностей здоровья"
              onChange={(event) => {setState({
                type: type,
                name: state.name,
                url: state.url,
                averageWeight: state.averageWeight,
                averageAge: state.averageAge,
                healthDescription: event.target.value,
                careDescription: state.careDescription,
                behaviourDescription: state.behaviourDescription,
                liked: "false",
                features: [""]
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="careD"
              label="Описание особенностей ухода"
              name="careD"
              autoComplete="Описание особенностей ухода"
              onChange={(event) => {setState({
                type: type,
                name: state.name,
                url: state.url,
                averageWeight: state.averageWeight,
                averageAge: state.averageAge,
                healthDescription: state.healthDescription,
                careDescription: event.target.value,
                behaviourDescription: state.behaviourDescription,
                liked: "false",
                features: [""]
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="behaviourD"
              label="Описание особенностей поведения"
              name="behaviourD"
              autoComplete="Описание особенностей поведение"
              onChange={(event) => {setState({
                type: type,
                name: state.name,
                url: state.url,
                averageWeight: state.averageWeight,
                averageAge: state.averageAge,
                healthDescription: state.healthDescription,
                careDescription: state.careDescription,
                behaviourDescription: event.target.value,
                liked: "false",
                features: [""]
              })}}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="features"
              label="Ключевые особенности (используйте ',*space*' для разделения)"
              name="features"
              autoComplete="Ключевые особенности"
              onChange={handleFeauturesChange}
            />
            <Button
            className={classes.add}
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleClickOnAdd}
            >
              Add
            </Button>
          </form>
        </div>
      </Container>
    );
}

export default AddPet