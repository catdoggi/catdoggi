import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';

import Pet from './Pet/Pet';
import useStyles from './styles';
import { Button, IconButton, MenuItem, Menu, Typography } from '@material-ui/core';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { FilterList} from '@material-ui/icons';
import { Link } from 'react-router-dom';

const PetsView = ({ pets, options, mode, changeMode, didClickOnFav, didClickCats, didClickDogs, didSelectFeature, deleteFeature }) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [haveFilter, setHaveFilter] = useState([]);
    const [feat, setFeat] = useState([]);
    const open = Boolean(anchorEl);
    const ITEM_HEIGHT = 48;

    function hadleFeature(feature) {
        setHaveFilter(true)
        didSelectFeature(feature)
        setFeat(feature)
    };

    function clickCats() {
        didClickCats()
    }

    function clickDogs() {
        didClickDogs()
    }

    function handleDelete() {
        setHaveFilter(false)
        setFeat("")
        deleteFeature()
    }

    const handleClickOutside = () => {
        setAnchorEl(null);
    }

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (feature) => () => {
        console.log('feature = ' + feature);
        hadleFeature(feature);
        setAnchorEl(null);
    };

    useEffect(() => {
        setFeat("");
        changeMode(mode)
        setHaveFilter(false)
        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
        }, []);

    return (
        <main className={classes.content}>
        <div className={classes.toolbar} />

        <Grid container justify="center" alignItems="center" spacing={4}>
            <Grid item xs={12} align="center">
            <ButtonGroup color="primary" size="large" label="button group">
                <Button component={Link} to={"/cats"} size="large" onClick={clickCats}>Cats</Button>
                <Button component={Link} to={"/dogs"} size="large" onClick={clickDogs}>Dogs</Button>
            </ButtonGroup>
            </Grid>
            <Grid item xs={12} align="left">
            <Typography gutterBottom variant="h5" component="h2">
            {'Apply filter'}
            
            <ButtonGroup color="primary" size="large" label="button group2">
                <IconButton aria-label="more" aria-controls="long-menu"
                            aria-haspopup="true" onClick={handleClick}>
                 {<FilterList /> }
                </IconButton>
                <Menu
                    id="long-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={handleClose}
                    PaperProps={{
                    style: {
                        maxHeight: ITEM_HEIGHT * 4.5,
                        width: '40ch',
                    },
                    }}
                >
                    {options.map((option) => (
                    <MenuItem key={option.feature} selected={option === 'Pyxis'} onClick={handleClose(option.feature)}>
                        {option.feature}
                    </MenuItem>
                    ))}
                </Menu>
                {haveFilter  &&
                    <Button size="large" onClick={handleDelete}>Delete filter</Button>
                }
            </ButtonGroup>
            </Typography>
            <Typography gutterBottom variant="b2" component="b2">
                    {feat}
            </Typography>
            </Grid>
            
            {pets.map((pet) => (
            <Grid key={pet.id} item xs={6}>
                <Pet pet={pet} didClickOnFav={didClickOnFav}/>
            </Grid>
            ))}
        </Grid>
        </main>
      );
};

export default PetsView;
