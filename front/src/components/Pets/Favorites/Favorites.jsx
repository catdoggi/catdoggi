import React, {useEffect} from 'react';
import useStyles from '../styles';

import Grid from '@material-ui/core/Grid';

import Pet from '../Pet/Pet';

const Favorites = ({pets, didClickOnFav, didClickOnLiked}) => {
    const classes = useStyles();

    function init() {
        didClickOnLiked()
    }

    useEffect(() => {
        init()
        console.log('pets =' +  pets)
      }, []);

    return(
        <main className={classes.content}>
        <div className={classes.toolbar} />

        <Grid container justify="center" alignItems="center" spacing={4}>
            <Grid item xs={12} align="center">
                { pets.length === 0 && (sessionStorage.getItem('myTokenName') === null || sessionStorage.getItem('myTokenName') === 'null') &&
                'Sign in for get your favourites'
                }
                { pets.length === 0 && (sessionStorage.getItem('myTokenName') !== null && sessionStorage.getItem('myTokenName') !== 'null') &&
                'Empty list'
                }
            </Grid>
            
            {pets.map((pet) => (
            <Grid key={pet.id} item xs={6}>
                <Pet pet={pet} didClickOnFav={didClickOnFav}/>
            </Grid>
            ))}
        </Grid>
        </main>
    );
}

export default Favorites;