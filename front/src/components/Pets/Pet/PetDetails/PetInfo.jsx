import React from 'react';
import { Grid, Typography, Box, Divider } from '@material-ui/core';

const PetInfo = ({ pet }) => {
    return (
    <Grid container direction="column" style={{height: "100%"}}>
        <Typography variant="h4">{pet.name}</Typography>
        <Divider/>
        <Box mt={3}>
            <Typography variant="h5">{"Вес: " + pet.averageWeight} </Typography>
            <Typography variant="h5">{"Средняя продолжительность жизни: " + pet.averageAge} </Typography>
        </Box>
    </Grid>
    )
};


export default PetInfo;