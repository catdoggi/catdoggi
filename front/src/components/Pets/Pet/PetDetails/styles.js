import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
    content: {
        flexGrow: 1,
        padding: 120,
    },
    root: {
        maxWidth: '100%',
    },
    media: {
        height: 0,
        paddingTop: '57%',
    },
    cardContent: {
        display: 'flex',
        justifyContent: 'space-between',
        space: 20,
    },
    gridRoot: {
        maxWidth: '100%', 
        margin: '0 auto',
    },
    cardActions: {
        display: 'flex',
        justifyContent: 'flex-end',
      },
}));