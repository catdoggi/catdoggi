import React, { useState, useEffect } from 'react';
import { Grid, Typography, Card, CardMedia, Divider, CardActions, IconButton } from '@material-ui/core';
import { DeleteOutline, Favorite, FavoriteBorder } from '@material-ui/icons';
import PetInfo from "./PetInfo.jsx";
import axios from 'axios';
import {Redirect} from 'react-router-dom'
import useStyles from './styles';

const PetDetails = ({animalId}) => {
  const classes = useStyles();
  const [pet, setPet] = useState([]);
  const [isAdmin, setAdmin] = useState({});
  const [isDelete, setDelete] = useState({});

  const handleClickOnFav = async() => {
    console.log("\ntoken: Bearer " + sessionStorage.getItem('myTokenName'));
    let param = 'like/' + animalId
    axios({
      method: 'POST',
      url: 'https://cat-doggi-back.herokuapp.com/animals/' + param,
      headers: {
        'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
      },       
    })
      .then(response => { 
        console.log(response)
      }) 
      .catch(error => console.log(error)) 
    fetchPet();
  }

  const checkAdmin = (token) => {
    axios({
        method: 'GET',
        url: 'https://cat-doggi-back.herokuapp.com/users/role',
        headers: {
          'Authorization': "Bearer " + token,
        }
      })
      .then(response => {
        setAdmin(response.data.admin);
      })
      .catch(error => console.log(error))
  }
  
  const fetchPet = async () => {
    console.log("\n token: Bearer " + sessionStorage.getItem('myTokenName'));
    axios({
      method: 'GET',
      url: 'https://cat-doggi-back.herokuapp.com/animals/' + animalId,
      headers: {
        'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
      },       
    })
    .then(response => response.data)
    .then(data => setPet(data))
    .catch(error => {
      console.log(error)
    });
  };

  const handleClickOnDelete = () => {
    console.log("\n token: Bearer " + sessionStorage.getItem('myTokenName'));
    axios({
      method: 'POST',
      url: 'https://cat-doggi-back.herokuapp.com/admin/animal/remove/' + animalId,
      headers: {
        'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
      },       
    })
    .then(response => {
      console.log("response: " + response);
      setDelete("true");
    })
    .catch(error => {
      console.log(error);
      setDelete("error");
    });
  }

  useEffect(() => {
    console.log("pet")
    window.scrollTo(0, 0)
    fetchPet();
    checkAdmin(sessionStorage.getItem('myTokenName'));
  }, []);

  //console.log(features);

  //console.log("isDelete: " + isDelete);
  if (isDelete === "true" || isDelete === true){
    return <Redirect push to="/"/>
  }
  
  return (
    <main className={classes.content}>
    <div>
    {(isDelete === "false" || isDelete === false) &&
      <h3 style={{ color: 'red' }}>Ошибка удаления</h3>
    }
    <Grid container spacing={5} className={classes.gridRoot}>
      <Grid item sm={5}>
        <Card className={classes.root}>
        <CardMedia className={classes.media} image={pet.url} title={pet.name}/>
        <CardActions disableSpacing className={classes.cardActions}>
        {(isAdmin === "true" || isAdmin === true) &&
        <IconButton aria-label="Delete pet" color="primary" onClick={handleClickOnDelete}>
          <DeleteOutline/>
        </IconButton>
        }
        <IconButton aria-label="Add to favorites" color="primary" onClick={handleClickOnFav}>
          { pet.liked ? <Favorite /> : <FavoriteBorder /> }
        </IconButton>
        </CardActions>
        </Card>
      </Grid>
      <Grid item sm={6}>
        <PetInfo pet={pet}/>
      </Grid>
      <Grid item sm={12} >
        <Typography variant="h5">Особенности здоровья</Typography> 
        <Typography variant="body2">{pet.healthDescription}</Typography> 
        <Divider/>
      </Grid>
      <Grid item sm={12} >
        <Typography variant="h5">Уход за питомцем</Typography> 
        <Typography variant="body2">{pet.careDescription}</Typography>
        <Divider/>
      </Grid>
      <Grid item sm={12} >
        <Typography variant="h5">Особенности поведения</Typography>
        <Typography variant="body2">{pet.behaviourDescription}</Typography> 
        <Divider/>
      </Grid>
      <Grid item sm={12} >
        <Typography variant="h5">Ключевые особенности животного</Typography>
        <ul>
        {pet.features && pet.features.map((feature) => (
          <li>{feature}</li>
        ))}
        </ul>
        <Divider/>
      </Grid>
    </Grid>
    </div>
    </main>
  )
};

export default PetDetails;