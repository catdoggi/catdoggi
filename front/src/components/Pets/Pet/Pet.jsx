import React from 'react';
import { Card, CardMedia, CardContent, CardActions, Typography, IconButton, Button } from '@material-ui/core';
import { Favorite, FavoriteBorder } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import axios from 'axios';

import useStyles from './styles';

const Pet = ({ pet, didClickOnFav }) => {
  const classes = useStyles();

  const handleClickOnFav = async() => {
    console.log("\ntoken: " + "Bearer " + sessionStorage.getItem('myTokenName'));
    let param = 'like/' + pet.animalID
    axios({
      method: 'POST',
      url: 'https://cat-doggi-back.herokuapp.com/animals/' + param,
      headers: {
        'Authorization': "Bearer " + sessionStorage.getItem('myTokenName'),
      },       
    })
      .then(response => { 
        console.log(response)
        didClickOnFav();
      }) 
      .catch(error => console.log(error)) 
       
  }
  
  return (
    <Card className={classes.root}>
      <CardMedia className={classes.media} image={pet.imgUrl} title={pet.name} />
      <CardContent>
        <div className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
            {pet.name}
          </Typography>
        </div>
      </CardContent>
      <CardActions disableSpacing className={classes.cardActions}>
        <Button component={Link} to={"/pet/" + pet.animalID} variant="contained" color="primary">
          View details
        </Button>
        <IconButton aria-label="Add to favorites" color="primary" onClick={handleClickOnFav}>
          { pet.likedByCurrentUser ? <Favorite /> : <FavoriteBorder /> }
        </IconButton>
      </CardActions>
    </Card>
  )
};

export default Pet;